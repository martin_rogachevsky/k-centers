from math import sqrt
from random import sample
from collections import defaultdict

class Classifier:
    MAX_ITER = 50

    def __init__(self, vectors, classesCount):
        self.classes = None
        self.classify(vectors, classesCount = classesCount)

    def classify(self, vectors, classesCount):
        centres = vectors[:classesCount]
        for _ in range(self.MAX_ITER):
            self.replaceCenters(vectors, centres)
            centres = set()
            for c in self.classes.values():
               centres.add(Classifier.center(vectors, c))
            if set(self.classes.keys()) == centres:
                break
    
    def replaceCenters(self, vectors, centers):
        classes = defaultdict(list)
        for vector_index, vector in enumerate(vectors):
            center = min(centers, key=lambda x: Classifier.distance(vector, x))
            classes[center].append(vector_index)
        self.classes = classes

    @staticmethod
    def sqr(a):
        return a*a

    @staticmethod
    def distance(a, b):
        return sqrt(sum(map(Classifier.sqr, (x-y for x, y in zip(a, b)))))

    @staticmethod
    def center(vectors, pointIdxs):
        count = len(vectors[0])
        result = [None]*count
        for vect in range(count):
            result[vect] = sum(vectors[i][vect] for i in pointIdxs) / len(pointIdxs)
        return tuple(result)