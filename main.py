from sys import argv, stderr
from plotBuilder import drawResults
from classifier import Classifier
import os
from random import randint

def main():
    classCount = int(argv[1])
    count = int(argv[2])
    vectors = []
    for i in range(count):
        vectors.append(tuple(randint(0, 100) for i in range(2)))
    if vectors:
        drawResults(vectors, Classifier(vectors, classCount).classes)

main()