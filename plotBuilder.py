import matplotlib.pyplot as plt
from random import random

def drawResults(vectors, classes):
    colors = []
    for _ in range(len(classes)):
        colors.append([random() for _ in range(3)])
    centerColors = []
    for color in colors:
        centerColors.append([1-x for x in color])
    for idx, (center, c) in enumerate(classes.items()):
        currClass = []
        for i in c:
            currClass.append(vectors[i])
            
        xs = list(map(lambda x: x[0], currClass))
        ys = list(map(lambda x: x[1], currClass))
        plt.scatter(xs, ys, c=colors[idx])
        plt.scatter(center[0], center[1], c=centerColors[idx], marker='x')
    plt.show()